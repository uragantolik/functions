const getSum = (str1, str2) => {
  if(typeof str1 !== 'string' || typeof str2 !== 'string')
    return false
  for (const c of str1) {
    if(isNaN(c))return false
  }
  for (const c of str2) {
    if(isNaN(c))return false
  }
  if(str1 < str2)
    [str1, str2] = [str2, str1]
  str1 = Array.from(str1).reverse().join('')
  str2 = Array.from(str2).reverse().join('')
  let memory = 0
  for(let i = 0; i < str1.length; ++i)
  {
    let sum = parseInt(str1[i].toString()) + (i >= str2.length ? 0 : parseInt(str2[i].toString())) + memory
    str1= str1.substring(0, i) + (sum % 10).toString() + str1.substring(i+1);
    memory = Math.trunc(sum/10) 
  }
  if(memory > 0)
      str1 += '1'
  return Array.from(str1).reverse().join('')
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postNum = 0
  let commentsNum = 0
  for (const post of listOfPosts) {
    if(post.author == authorName)
      postNum++
    if(post.comments)
      for (const comment of post.comments) {
        if(comment.author == authorName)
          commentsNum++
      }
  }
  return `Post:${postNum},comments:${commentsNum}`;
};

const tickets=(people) => {
  let money = {'25': 0, '50': 0, '100': 0}
  for (const bill of people) {
      switch (bill) {
        case 25:
          money['25']++
          break;
        
        case 50:
          money['25']--
          money['50']++
          break;

        case 100:
          if(money['50'] == 0) {
            money['25'] -= 3
            money['100']++;
          }
          else {
            money['25']--
            money['50']--
            money['100']++
          }
          break;
      }
      if(money['25'] < 0) 
        return 'NO'
  }
  return 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
